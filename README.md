# Word Slides component

## Information for reviewer

**WordSlidesApp** is the main component. It does not implement Callable (and is not annotated with @Async) however it 
was made with a thought that it may be accessed by many threads concurrently i.e. its global variables/instances was
 made thread-safe.

Application contains two implementations of Store however only RandomStoreImpl is scanned and injected to WordSlidesApp

- **RandomStoreImpl** - completely random and not idempotent
- **RandomCachedStoreImpl** - I wrote it before store's working principle was clarified and decided to keep it. This 
is also random implementation however it stores information about what slides the store was already checked against 
and ensures that the same information about slide's presence is returned for a 
given slide. 

**AsyncIntGenerator** class is used for generating random Integer from 1 to 10. It is simulating that the operation 
is time-consuming by pausing the thread for 2 seconds

I created tests using both standard TDD approach (JUnit/Mockito) but I have also attached functionally equivalent tests 
in Spock (these are my first test using Spock so they should not be considered production ready :) )

**CommandLiner** was created in order to be able to test if WordSlidesApp component with real (not-mocked) store is 
working fine.