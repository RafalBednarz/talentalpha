package com.talentalpha.codingtask;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.talentalpha.codingtask.store.AsyncIntGenerator;
import com.talentalpha.codingtask.store.Store;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

/**
 * Slides App that exposes one public method which generates 'slides' for given input, check them against slides
 * in a 'store' and returns existing slides as a keys and corresponding values as integers from 1 to 10.
 * Store and Integer generator implementations are injected as dependencies.
 * */
@Component
@Slf4j
public final class WordSlidesApp {
    private static final String ONE_OR_MORE_SPACES_REGEX = "\\s+";

    private Store store;
    private AsyncIntGenerator asyncIntGenerator;

    @Autowired
    public WordSlidesApp(Store store, AsyncIntGenerator asyncIntGenerator) {
        this.store = store;
        this.asyncIntGenerator = asyncIntGenerator;
    }

    /**
     * @param inputText Any input text for which 'slides' will be generated. Slides are set of words delimited by
     *                  space/spaces. Input text may contain multiple spaces between words, but they will be treated
     *                  in the same way as single space. Leading and trailing spaces will be ignored.
     * @return map which keys will be slides present in a 'Store'. Value will be a random integer from 1 to 10.
     * If duplicate slides are found in the store then only one will be in a map (because keys must be unique) with
     * random integer generated for the latest duplicate.
     */
    public Map<String, Integer> getSlidesToRandomInt(String inputText) {
        log.info("Invoked getSlidesToRandomInt with an input: {}", inputText);
        if (inputText == null) {
            return ImmutableMap.of();
        }

        Map slideToRandomInt = new HashMap();

        ImmutableList<Word> inputWords = populateInputAsListOfWords(inputText);

        ImmutableList<CompletableFuture<AsyncIntGenerator.SlideAndRandomInt>>
                slideToRandomIntFuture = getAllSlidesAndTriggerCalculatingRandomInt(inputWords);

        for (CompletableFuture<AsyncIntGenerator.SlideAndRandomInt> future : slideToRandomIntFuture) {
            try {
                AsyncIntGenerator.SlideAndRandomInt slideAndInteger = future.get();
                slideToRandomInt.put(slideAndInteger.getSlide(), slideAndInteger.getRandomNumber());
            } catch (InterruptedException | ExecutionException e) {
                log.error("Unexpected error occured while generating random integer.");
                throw new RuntimeException(e);
            }
        }
        return slideToRandomInt;
    }

    private ImmutableList<CompletableFuture<AsyncIntGenerator.SlideAndRandomInt>> getAllSlidesAndTriggerCalculatingRandomInt(ImmutableList<Word> wordsInOrder) {

        ImmutableList.Builder<CompletableFuture<AsyncIntGenerator.SlideAndRandomInt>>
                slideToRandomIntFuture = new ImmutableList.Builder<>();
        for (int slideLength = wordsInOrder.size(); slideLength > 0; slideLength--) {
            log.debug("Checking slides for slide length {}", slideLength);
            slideToRandomIntFuture.addAll(getSlidesForGivenNumberOfWords(wordsInOrder, slideLength));
        }
        return slideToRandomIntFuture.build();
    }

    private ImmutableList<Word> populateInputAsListOfWords(String inputText) {
        ImmutableList<String> inputSplitBySpaces =
                ImmutableList.copyOf(inputText.strip().split(ONE_OR_MORE_SPACES_REGEX));
        ImmutableList.Builder<Word> wordsInOrder = new ImmutableList.Builder();
        for (String word : inputSplitBySpaces) {
            wordsInOrder.add(new Word(word, false));
        }
        return wordsInOrder.build();
    }

    private ImmutableList<CompletableFuture<AsyncIntGenerator.SlideAndRandomInt>> getSlidesForGivenNumberOfWords(ImmutableList<Word> list, int numberOfWords) {
        ImmutableList.Builder<CompletableFuture<AsyncIntGenerator.SlideAndRandomInt>>
                resultFutures = new ImmutableList.Builder<>();
        for (int beginIndex = 0; beginIndex + numberOfWords <= list.size(); beginIndex++) {
            ImmutableList<Word> slide = list.subList(beginIndex, beginIndex + numberOfWords);

            String slideAsString = concatStringsWithSpaceDelimiter(slide);
            if (canSlideBeUsed(slide) && store.isSlidePresent(slideAsString)) {
                crossOutConsumedWords(slide);
                resultFutures.add(asyncIntGenerator.generateRandomInteger(slideAsString));
            }
        }
        return resultFutures.build();
    }

    private boolean canSlideBeUsed(ImmutableList<Word> slide) {
        return !slide.stream().filter(w -> w.isUsed()).findAny().isPresent();
    }

    private void crossOutConsumedWords(ImmutableList<Word> slide) {
        for (Word w : slide) {
            log.debug("crossing out word {}", w.getText());
            w.setUsed(true);
        }
    }

    private String concatStringsWithSpaceDelimiter(ImmutableList<Word> slide) {
        return String.join(" ", slide.stream().map(word -> word.getText()).collect(Collectors.toList()));
    }

}
