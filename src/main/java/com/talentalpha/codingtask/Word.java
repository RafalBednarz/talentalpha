package com.talentalpha.codingtask;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@AllArgsConstructor
@Getter
final class Word {

    @NonNull
    private final String text;
    @Setter
    private boolean used;
}
