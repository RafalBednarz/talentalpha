package com.talentalpha.codingtask;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class CommandLiner implements CommandLineRunner {

    @Autowired
    private WordSlidesApp slidesApp;

    public CommandLiner(WordSlidesApp slidesApp) {
        this.slidesApp = slidesApp;
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println(slidesApp.getSlidesToRandomInt("Mary went Mary's gone"));
    }

}