package com.talentalpha.codingtask.store;

public interface Store {
    /**
     *
     * @param slide which presence is checked
     * @return true if slide is present in store, false otherwise
     */
    boolean isSlidePresent(String slide);
}
