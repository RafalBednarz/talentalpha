package com.talentalpha.codingtask.store;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.concurrent.ThreadSafe;
import java.util.Random;

/**
 * Store implementation that returns slide's presence at random.
 */
@Component
@Slf4j
@ThreadSafe
public final class RandomStoreImpl implements Store {

    @Override
    public synchronized boolean isSlidePresent(String slide) {
            return new Random().nextBoolean();
    }
}
