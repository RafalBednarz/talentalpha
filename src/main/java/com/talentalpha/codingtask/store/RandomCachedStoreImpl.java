package com.talentalpha.codingtask.store;

import javax.annotation.concurrent.ThreadSafe;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Random store implementation which is idempotent i.e. for a given slide it will always return
 * the same information about slide's presence.
 */

@ThreadSafe
public class RandomCachedStoreImpl implements Store {

    private Map<String, Boolean> storeContent = new HashMap<>();

    @Override
    public synchronized boolean isSlidePresent(String slide) {
        if (storeContent.get(slide) == null) {
            Random random = new Random();
            storeContent.put(slide, random.nextBoolean());
            return random.nextBoolean();
        } else {
            return storeContent.get(slide);
        }
    }
}
