package com.talentalpha.codingtask.store;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.annotation.concurrent.ThreadSafe;
import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.locks.LockSupport;


@Slf4j
@Component
@ThreadSafe
public class AsyncIntGenerator {
    public static final int TWO_SECONDS = 2_000_000_000;

    @Async
    public CompletableFuture<SlideAndRandomInt> generateRandomInteger(String slide) {
        log.info("Generating random number for slide {}", slide);
        // This is to simulate long-lasting operation LockSupport is used here instead of Thread.sleep to
        // avoid handing InterruptedException which should never happen here anyway
        LockSupport.parkNanos(TWO_SECONDS);
        Integer randomNumber = new Random().nextInt(9) + 1;
        log.info("Random number {} generated for slide {}", randomNumber, slide);
        SlideAndRandomInt slideAndRandomInt = new AsyncIntGenerator.SlideAndRandomInt(slide, randomNumber);
        return CompletableFuture.completedFuture(slideAndRandomInt);
    }

    @Getter
    @AllArgsConstructor
    public static class SlideAndRandomInt {
        @NonNull
        private final String slide;
        private final int randomNumber;
    }
}
