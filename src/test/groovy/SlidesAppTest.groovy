import com.talentalpha.codingtask.WordSlidesApp
import com.talentalpha.codingtask.store.AsyncIntGenerator
import com.talentalpha.codingtask.store.Store
import spock.lang.Specification

/* This is experimental Spock's test suite, all of tests are functionally equivalent
to JUnit tests from test/java folder, however some tests take longer than its counterparts
in JUnit/Mockito because of lack Spring's configuration and @EnableAsync annotation
(I didn't have enough time to check how to make it work in Groovy) */

class SlidesAppTest extends Specification {
    def single_digit_int = [1,2,3,4,5,6,7,8,9];
    Store mockStore
    WordSlidesApp app

    def setup() {
        mockStore = Stub(Store)
        app = new WordSlidesApp(mockStore, new AsyncIntGenerator())
    }

    def 'should return empty collection when null'() {
        when:
        Map<String, Integer> slideToInt = app.getSlidesToRandomInt(null)

        then:
        slideToInt.size()==0
    }

    def 'Only one word which is in store is passed as input'() {
        given:
        mockStore.isSlidePresent('Mary') >> true

        when:
        Map<String, Integer> slideToInt = app.getSlidesToRandomInt('Mary')

        then:
        slideToInt.size()==1
        single_digit_int.contains(slideToInt.get('Mary'))
    }

    def 'Only one word which is NOT in store is passed as input'() {
        given:
        mockStore.isSlidePresent('Mary') >> false

        when:
        Map<String, Integer> slideToInt = app.getSlidesToRandomInt('Mary')

        then:
        slideToInt.size()==0
    }

    def 'Two words passed, both present as a slide but separately'() {
        given:
        mockStore.isSlidePresent('Mary went') >> false
        mockStore.isSlidePresent('Mary') >> true
        mockStore.isSlidePresent('went') >> true

        when:
        Map<String, Integer> slideToInt = app.getSlidesToRandomInt('Mary went')

        then:
        slideToInt.size()==2
        single_digit_int.contains(slideToInt.get('Mary'))
        single_digit_int.contains(slideToInt.get('went'))
    }

    def 'Two words passed, and whole input present as a slide'() {
        given:
        mockStore.isSlidePresent('Mary went') >> true

        when:
        Map<String, Integer> slideToInt = app.getSlidesToRandomInt('Mary went')

        then:
        slideToInt.size()==1
        single_digit_int.contains(slideToInt.get('Mary went'))
    }

    def 'Test example from task description'() {
        given:
        // simulate only behaviour of "present" slides explicitly, for slides not in store
        // default boolean's value "false" is returned
        mockStore.isSlidePresent('went Mary\'s') >> true
        mockStore.isSlidePresent('Mary') >> true

        when:
        Map<String, Integer> slideToInt = app.getSlidesToRandomInt('Mary went Mary\'s gone')

        then:
        slideToInt.size()==2
        single_digit_int.contains(slideToInt.get('went Mary\'s'))
        single_digit_int.contains(slideToInt.get('Mary'))
    }

    def 'Trailing and leading spaces are present as well as multiple spaces between words'() {
        given:
        mockStore.isSlidePresent('Mary went') >> false
        mockStore.isSlidePresent('Mary') >> true
        mockStore.isSlidePresent('went') >> true
        mockStore.isSlidePresent('') >> true

        when:
        Map<String, Integer> slideToInt = app.getSlidesToRandomInt(' Mary  went ')

        then:
        slideToInt.size()==2
        single_digit_int.contains(slideToInt.get('Mary'))
        single_digit_int.contains(slideToInt.get('went'))
    }

    def 'Duplicate was consumed twice'() {
        given:
        mockStore.isSlidePresent('Mary went') >> true

        when:
        Map<String, Integer> slideToInt = app.getSlidesToRandomInt('Mary went Mary went gone')

        then:
        slideToInt.size()==1
        single_digit_int.contains(slideToInt.get('Mary went'))
    }

    def 'Duplicate was consumed only once because store is NOT idempotent'() {
        given:
        mockStore.isSlidePresent('Mary went') >> true >> false
        mockStore.isSlidePresent('Mary') >> true
        mockStore.isSlidePresent('went') >> true

        when:
        Map<String, Integer> slideToInt = app.getSlidesToRandomInt('Mary went Mary went gone')

        then:
        slideToInt.size()==3
        single_digit_int.contains(slideToInt.get('Mary went'))
        single_digit_int.contains(slideToInt.get('Mary'))
        single_digit_int.contains(slideToInt.get('went'))
    }


}  