package com.talentalpha.codingtask;

import com.talentalpha.codingtask.store.AsyncIntGenerator;
import com.talentalpha.codingtask.store.Store;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;

import static junit.framework.TestCase.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class SlidesAppTest {

    @Configuration
    @EnableAsync
    static class Config {
        @Bean
        public AsyncIntGenerator asyncIntGenerator() {
            return new AsyncIntGenerator();
        }
        @Bean
        public Executor taskExecutor() {
            return new SimpleAsyncTaskExecutor();
        }
    }

    @Autowired
    private AsyncIntGenerator asyncIntGenerator;

    @Mock
    private Store mockStore;
    private WordSlidesApp app;

    private static final List<Integer> INT_FROM_1_TO_10 = List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

    @Before
    public void setUp() {
        app = new WordSlidesApp(mockStore, asyncIntGenerator);
    }

    @Test
    public void nullAsAnInputParam() {
        assertTrue(app.getSlidesToRandomInt(null).isEmpty());
    }

    @Test
    public void singleWordPassedAndPresentInStore() {
        when(mockStore.isSlidePresent("Mary")).thenReturn(true);
        Map map = app.getSlidesToRandomInt("Mary");

        assertMapContainsExactlyGivenKeys(map, "Mary");
    }

    @Test
    public void singleWordPassedWhichIsNotInStore() {
        when(mockStore.isSlidePresent("John")).thenReturn(false);

        Map map = app.getSlidesToRandomInt("John");

        assertTrue(map.size() == 0);
    }

    @Test
    public void twoWordsBothInStoreSeparately() {
        when(mockStore.isSlidePresent("Mary went")).thenReturn(false);
        when(mockStore.isSlidePresent("Mary")).thenReturn(true);
        when(mockStore.isSlidePresent("went")).thenReturn(true);

        Map map = app.getSlidesToRandomInt("Mary went");

        assertMapContainsExactlyGivenKeys(map, "Mary", "went");
    }

    @Test
    public void twoWordsAsASingleSlideInStore() {
        when(mockStore.isSlidePresent("Mary went")).thenReturn(true);

        Map map = app.getSlidesToRandomInt("Mary went");

        assertMapContainsExactlyGivenKeys(map, "Mary went");
    }

    @Test
    public void testCaseFromCodingTaskDescription() {
        // simulate only behaviour of "present" slides explicitly, for slides not in store
        // default boolean's value "false" is returned
        when(mockStore.isSlidePresent("went Mary's")).thenReturn(true);
        when(mockStore.isSlidePresent("Mary")).thenReturn(true);

        Map map = app.getSlidesToRandomInt("Mary went Mary's gone");

        assertMapContainsExactlyGivenKeys(map, "went Mary's", "Mary");
    }

    @Test
    public void trailingAndLeadingSpacesAreTrimmedAndMultipleSpacesAreSupported() {
        when(mockStore.isSlidePresent("Mary went")).thenReturn(false);
        when(mockStore.isSlidePresent("Mary")).thenReturn(true);
        when(mockStore.isSlidePresent("went")).thenReturn(true);
        when(mockStore.isSlidePresent("")).thenReturn(true);


        Map map = app.getSlidesToRandomInt(" Mary  went ");

        assertMapContainsExactlyGivenKeys(map, "Mary", "went");
    }

    @Test
    public void duplicateIsConsumedTwice() {
        when(mockStore.isSlidePresent("Mary went")).thenReturn(true);

        Map map = app.getSlidesToRandomInt("Mary went Mary went gone");

        assertMapContainsExactlyGivenKeys(map, "Mary went");
    }

    @Test
    public void duplicateWasConsumedOnceForNotIdempotentStore() {
        when(mockStore.isSlidePresent("Mary went")).thenReturn(true).thenReturn(false);
        when(mockStore.isSlidePresent("Mary")).thenReturn(true);
        when(mockStore.isSlidePresent("went")).thenReturn(true);

        Map map = app.getSlidesToRandomInt("Mary went Mary went gone");

        assertMapContainsExactlyGivenKeys(map, "Mary went", "Mary", "went");
    }

    private void assertMapContainsExactlyGivenKeys(Map map, String... keys) {
        assertTrue(map.size() == keys.length);
        for (String key : keys) {
            assertTrue(INT_FROM_1_TO_10.contains(map.get(key)));
        }
    }
}
